from TarantulaRPC import BasicType, TarantulaRPC
import sys


# 用户自定义部分，包括定于数据类型和执行函数
def define_functions(rpc):
    rpc.define_data_type("d1", {
        "inta": BasicType.Uint32,
        "intb": BasicType.SBint32,
        "stringc": BasicType.String
    })
    rpc.define_data_type("s", {"s": BasicType.String})

    # 自定义函数，并规定其输入输出类型
    # 输入为指定变量数据类型的参数，这里为d1, 注:参数名称必须与数据类型定义名称相同
    # 输出为字典，格式同样与数据类型定义时相同
    # 自定义函数首字母需要大写
    def Test_func(inta, intb, stringc):
        return {"s": str(inta) + str(intb) + stringc}

    rpc.add_function(Test_func, "d1", "s")


if __name__ == "__main__":
    # 服务端和客户端运行前需要预先编译生成TarantulaRPC_pb2.py和TarantulaRPC_pb2_grpc.py
    # 生成的TarantulaRPC.proto可以跨语言使用，运行时非必须
    if sys.argv[1] == "compile":
        rpc = TarantulaRPC("test_grpc")
        define_functions(rpc)
        rpc.compile_proto()
        print("编译完成！")
    elif sys.argv[1] == "server":
        rpc = TarantulaRPC("test_grpc")
        define_functions(rpc)
        rpc.run_server()
