from enum import Enum, unique
import os
import grpc
import time
import requests
import traceback
from concurrent import futures


@unique
class BasicType(Enum):
    """
    基础类型，对应GRPC定义的数据类型
    """
    Double = "double"
    Float = "float"
    String = "string"
    Bool = "bool"
    Bytes = "bytes"
    # 各种INT
    Uint32 = "uint32"  # 无符号INT32
    Uint64 = "uint64"  # 无符号INT64
    Sint32 = "sint32"  # 有符号INT32
    Sint64 = "sint64"  # 有符号INT64
    UBint32 = "fixed32"  # 无符号INT32，固定占用4字节，推荐数值经常大于2^28时使用
    UBint64 = "fixed64"  # 无符号INT64，固定占用8字节，推荐数值经常大于2^56时使用
    SBint32 = "sfixed32"  # 有符号INT32，固定占用4字节，与上面同理
    SBint64 = "sfixed64"  # 有符号INT64，固定占用8字节，与上面同理


class TarantulaRPC:
    __format = "syntax = \"proto3\";\npackage spiderpc;\nservice {service_name}{{\n\t{function_string}\n}}\n{data_type_string}"
    __function_format = "rpc {name} ({input_type}) returns ({output_type}) {{}}"
    __date_type_format = "message {data_type_name}{{\n\t{base_type_string}\n}}\n"

    def __init__(self, name):
        self.name = name
        self.__data_type = {}

        # 请不要自行修改这个变量
        self.functions = {}

    def define_data_type(self, name: str, define: dict):
        """
        定义一种用于输入输出的数据类型
        :param name: 数据类型名称
        :param define: 字典，结构为{属性名:基础数据类型},其中基础数据类型必须为BasicType
        """
        try:
            assert type(define) == dict
            for t in define:
                assert type(define[t]) == BasicType
        except:
            raise TypeError("'define' should be a list of BasicType")

        self.__data_type[name] = define

    def add_function(self, function, input: str, output: str):
        """
        添加一个自定义函数，并规定其输入输出类型
        :param function: 指定的函数，function对象
        :param input: 输入的数据类型
        :param output: 输出的数据类型
        """
        if input not in self.__data_type or output not in self.__data_type:
            raise TypeError("input and output must be defined data type")
        self.functions[function.__name__.capitalize()] = (function, input, output)

    # 构建并写入proto文件
    def generate_proto(self):
        proto = self.__format.format(
            service_name="TarantulaRPC",
            function_string="\n\t".join([
                self.__function_format.format(
                    name=f,
                    input_type=self.functions[f][1],
                    output_type=self.functions[f][2]
                ) for f in self.functions
            ]),
            data_type_string="\n".join([
                self.__date_type_format.format(
                    data_type_name=t,
                    base_type_string="\n\t ".join(
                        ["%s %s = %d;" % (self.__data_type[t][bt].value, bt, index + 1) for index, bt in
                         enumerate(self.__data_type[t])])
                ) for t in self.__data_type
            ])
        )

        with open("%s.proto" % self.name, "w", encoding="utf-8") as f:
            f.write(proto)

    # 生成并编译proto
    def compile_proto(self):
        self.generate_proto()
        os.system("python -m grpc_tools.protoc -I ./ --python_out=./ --grpc_python_out=./ %s.proto" % self.name)

    # 运行GRPC服务器
    def run_server(self, module=None, host="0.0.0.0", port=1234, workers=3, tls=False, health_report=None):
        """
        启动RPC服务器
        :param module: RPC服务器模块名称，对应self.name和编译后的文件名称前缀([module]_pb2), 默认为self.name
        :param host: 监听地址
        :param port: 监听端口
        :param workers: 最大并发线程数
        :param tls: 是否开启TLS。开启TLS需要服务端和客户端都配置CN为服务端域名的证书文件，服务端额外需要私钥文件
        :param health_report: 是否定时进行健康上报，如果进行，该字段为需要POST请求的HTTP/HTTPS地址
        :return: 进程阻塞，Ctrl+C正常退出
        """
        if module is None:
            module = self.name
        module_pb2 = __import__(module + "_pb2")
        module_pb2_grpc = __import__(module + "_pb2_grpc")
        rpc = self

        class CRPCServicer:
            def __getattr__(self, item):
                handler, in_type, out_type = rpc.functions.get(item)

                def proxy(input, ctx):
                    output = handler(**{item[0].name: item[1] for item in input.ListFields()})
                    return getattr(module_pb2, out_type)(**output)

                return proxy

        server = grpc.server(futures.ThreadPoolExecutor(max_workers=workers))
        servicer = CRPCServicer()
        module_pb2_grpc.add_TarantulaRPCServicer_to_server(servicer, server)
        # TLS加密
        if tls:
            with open('server.key', 'rb') as f:
                private_key = f.read()
            with open('server.crt', 'rb') as f:
                certificate_chain = f.read()
            server_credentials = grpc.ssl_server_credentials(
                ((private_key, certificate_chain,),))
            server.add_secure_port(f'{host}:{port}', server_credentials)
        else:
            server.add_insecure_port(f'{host}:{port}')

        server.start()

        try:
            print("TarantulaRPC is running...")
            if health_report:
                data = {
                    "port": port,
                    "workers": workers,
                    "module": self.name,
                    "handlers": {func: list(self.functions[func][1:]) for func in self.functions},
                    "data_type": {dt: {t: self.__data_type[dt][t].value for t in self.__data_type[dt]} for dt in
                                  self.__data_type},
                    "tls": tls,
                }
                while True:
                    try:
                        response = requests.post(health_report, json=data)
                        if response.status_code // 100 != 2:
                            print(response.text)
                        else:
                            print("Succeed in health report!")
                    except:
                        traceback.print_exc()
                    time.sleep(10)
            else:
                while True:
                    time.sleep(1000)
        except KeyboardInterrupt:
            print("stopping...")
            server.stop(0)

    @staticmethod
    def run(target: str, module: str, handler: str, input_type: str, input: dict):
        """
        作为客户端，运行某个目标函数，同步阻塞至返回
        :param target: 目标地址字符串，如"127.0.0.1:1234"
        :param module: 模块名称
        :param handler: 调用的函数名称
        :param input_type: 输入数据的数据类型
        :param input: 输入数据字典
        :return: 与输出数据类型结构相同的字典
        """
        module_pb2 = __import__(module + "_pb2")
        module_pb2_grpc = __import__(module + "_pb2_grpc")
        with grpc.insecure_channel(target) as channel:
            client = module_pb2_grpc.TarantulaRPCStub(channel=channel)
            response = getattr(client, handler)(getattr(module_pb2, input_type)(**input))
            return {item[0].name: item[1] for item in response.ListFields()}

    @staticmethod
    def run_async(target: str, module: str, handler: str, input_type: str, input: dict, callback, tls=False):
        """
        作为客户端，运行某个目标函数，即时返回，异步运行
        :param target: 目标地址字符串，如"127.0.0.1:1234"
        :param module: 模块名称
        :param handler: 调用的函数名称
        :param input_type: 输入数据的数据类型
        :param input: 输入数据字典
        :param callback: 完成回调函数
        :return: channel -- RPC连接，记得及时关闭
                 future对象，可以用future.result()获取结果&&捕获错误

        """
        module_pb2 = __import__(module + "_pb2")
        module_pb2_grpc = __import__(module + "_pb2_grpc")
        if tls:
            with open('server.crt', 'rb') as f:
                trusted_certs = f.read()
            credentials = grpc.ssl_channel_credentials(root_certificates=trusted_certs)
            channel = grpc.secure_channel(target, credentials)
        else:
            channel = grpc.insecure_channel(target)
        client = module_pb2_grpc.TarantulaRPCStub(channel=channel)
        call_future = getattr(client, handler).future(getattr(module_pb2, input_type)(**input))
        call_future.add_done_callback(callback)
        return channel, call_future
