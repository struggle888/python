from TarantulaRPC import TarantulaRPC
from dataclasses import dataclass
import time


@dataclass(frozen=True)
class RPC_Request:
    host: str
    port: int
    module: str
    handler: str
    input_type: str
    input_data: dict


def call_multiRPC(rpcs: list) -> list:
    """
    同时调用多个RPC，阻塞至全部返回
    :param rpcs: [RPC_Request]
    :return:
    """
    result = []

    def rpc_callback(future):
        try:
            result.append({item[0].name: item[1] for item in future.result().ListFields()})
        except:
            # 错误处理
            result.append("Error in:" + str(future.target) + ", " + future._state.__dict__["details"])
        # 必须close
        future.channel.close()

    for rpc in rpcs:
        # 单独调用一个RPC，这一行即可
        channel, f = TarantulaRPC.run_async(f'{rpc.host}:{rpc.port}', rpc.module, rpc.handler, rpc.input_type,
                                            rpc.input_data, rpc_callback)
        f.target = rpc
        f.channel = channel

    while len(result) < len(rpcs):
        time.sleep(1)
    return result


if __name__ == "__main__":
    rpcs = [RPC_Request("127.0.0.1", 1234, "test_grpc", "Test_func", "d1",
                        {"inta": 66666, "intb": -2100000000, "stringc": "tests"})]
    print("-", call_multiRPC(rpcs), "-")
