from odps.tunnel import TableTunnel
from odps import ODPS
import time
from multiprocessing import Process
import traceback
import os
import threading
import queue

"""
功能：ODPS大数据上传
特性:
1. 对于单个文件自定义线程数多线程上传
2. 可以多进程同时对多个文件上传
3. 可设定每个文件上传超时，超时自动重试（避免上传卡住等问题）
4. 实现了监控当前目录的特征文件，并自动上传，上传后删除
5. 可兼容其它情况、自定义上传格式（不只是静态文件上传，因为本质是生成器）
6. 程序已经稳定运行数百小时，稳定性有所保证
注意：
1. 代码只是参考，并没有做太多对其他功能、场景的兼容性处理
2. 文件缺少日志模块，很多地方靠print进行输出
3. 代码不是很多，注释也补了很多，最好全部看懂后修改

By Druggle 20.6.30
"""

# --配置--
# 进程限制--最多同时上传的文件个数
PROCESS_LIMIT = 2
# 线程数--上传每个文件的线程数，推荐2-50之间
WORKER_AMOUNT = 10
# 上传每个文件的超时，单位秒，超时会自动kill、重试
TIMEOUT = 600

# 主类
class UploadRecords:
    def __init__(self, *args, **kwargs):
        self.odps = ODPS(*args, **kwargs)
        self.table_name = ""
        self.data_cache = []
        self.worker_amount = WORKER_AMOUNT
        self.partition = None

    # 设置分区，会自动创建
    def set_partition(self, partition):
        self.partition = partition
        table = self.odps.get_table(self.table_name)
        table.create_partition(partition, if_not_exists=True)

    # 多线程开始上传
    # 参数:
    #     generator: 一个生成器对象
    def run_thread(self, generator):
        # 检查参数
        if not self.table_name:
            return

        #初始化变量
        tunnel = TableTunnel(self.odps)
        upload_session = tunnel.create_upload_session(self.table_name, partition_spec=self.partition)
        session_id = upload_session.id
        table = self.odps.get_table(self.table_name)
        worker_amount = self.worker_amount
        # 队列缓冲为10000*worker_amount
        q = queue.Queue(10000*worker_amount)

        # 创建worker
        print("创建worker...")
        threads = []
        for index in range(worker_amount):
            t = threading.Thread(target=write_records_thread, args=(tunnel, session_id, index, self.table_name, table, q, self.partition))
            t.start()
            threads.append(t)

        # 不断地推数据
        n = 0
        while True:
            try:
                item = next(generator)
            except StopIteration:
                break
            q.put(item)
            n+=1
            if n % (1000*worker_amount) == 0:
                print("已推入%d条数据!" % n)
        print("已推完全部数据")
        # 发送结束条件
        for _ in range(worker_amount):
            q.put(False)


        # 等待结束
        for t in threads:
            t.join()
        print("所有线程已结束，等待commit")
        upload_session.commit(list(range(worker_amount)))
        print("END!")


# 上传子线程
def write_records_thread(tunnel, session_id, block_id, table_name, table, q, partiton):
    try:
        local_session = tunnel.create_upload_session(table_name, upload_id=session_id, partition_spec=partiton)
        with local_session.open_record_writer(block_id) as writer:
            while True:
                item = q.get()
                if item is False:
                    break
                writer.write(table.new_record(item))
    except:
        traceback.print_exc()

# 创建一个限时进程
def time_limit_process(target, timeout, *args, **kwargs):
    p = Process(target=target, args=args, kwargs=kwargs)
    p.daemon = True
    p.start()
    p.join(timeout=timeout)
    if p.is_alive():
        print("超时中断！")
        p.terminate()
        time.sleep(3)
    while p.is_alive():
        p.kill()
    global RUNNING_PROCESS
    RUNNING_PROCESS.remove(args[0])
    print("End file:", args[0])




# 上传单个文件
def upload_single_file(filename, table_name, *args, **kwargs):
    uploader = UploadRecords(*args, **kwargs)
    uploader.table_name = table_name

    # 设置分区，这里是日期8位，可自定义
    uploader.set_partition("update_date=%s" % time.strftime("%Y%m%d", time.localtime()))

    # 调用run_thread执行上传
    uploader.run_thread(item_generator(filename))

    # 上传后删除
    os.remove(filename)

# 监控当前目录
RUNNING_PROCESS = []
def monitor_current_dir_by_multiprocess(table, *args, **kwargs):
    import os
    global RUNNING_PROCESS
    global TIMEOUT
    global PROCESS_LIMIT
    while True:
        files = os.listdir(os.getcwd())
        for f in files:
            # 文件特征匹配【可自定义】 + 防止多个进程上传一个文件
            if f.startswith("certs-") and f.endswith(".txt") and f not in RUNNING_PROCESS:
                print("Get file:", f)
                thd = threading.Thread(target=time_limit_process, args=tuple([upload_single_file, TIMEOUT, f, table] + list(args)), kwargs=kwargs)
                thd.setDaemon(True)
                thd.start()
                RUNNING_PROCESS.append(f)
                while len(RUNNING_PROCESS) >= PROCESS_LIMIT:
                    time.sleep(10)
                else:
                    break

        time.sleep(10)

# ---------------------------------- 自定义内容----------------------------------------
# 示例生成器
# 生成器生成的对象需要为一个list，每一项依次对应ODPS表中的一列，其中ODPS中Timestamp类型对应datetime.datetime类型
def item_generator(filename):
    f = open(filename)
    for line in f:
        yield line[:-1].split(" ")
    f.close()


if __name__ == '__main__':
    monitor_current_dir_by_multiprocess("<TableName>", "<AccessKey>", "<AccessToken>", "<Project>",
                    endpoint="<EndPoint>", tunnel_endpoint="<TunnelEndpoint>")